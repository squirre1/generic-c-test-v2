#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#include <string.h>


#include <string>

void usage() {
    printf("./ledserver -i <ip addr> -p <port> command [arg]\n");
    printf("\n");
    printf("example:\n");
    printf("./ledserver -i 127.0.0.1 -p 3000 get-led-state\n");
    printf("./ledserver -i 127.0.0.1 -p 3000 set-led-color red\n");
}


int main(int argc, char **argv)
{
    int c;
    
    std::string ip = "0.0.0.0";
    int port = 0;
    
    while((c = getopt(argc, argv, "i:p:h")) != -1) {
        switch(c) {
            case 'i':
                ip = optarg;
                break;
            case 'p':
                port = atoi(optarg);
                break;
            case 'h':
                usage();
                exit(EXIT_SUCCESS);
                break;
            default:
                break;
        }
    }

    std::string command_str;
    for (int index = optind; index < argc; index++) {
        command_str += argv[index] + std::string(" ");
    }
    if (command_str.size() > 0) {
        command_str.pop_back();
    }

    
    int error_save = 0;   // Нашел в интернете такой способ сохранения errno
    int fd = socket(AF_INET, SOCK_STREAM, 0);


    // Таймаут чтения
    struct timeval tv;
    tv.tv_sec = 20;
    tv.tv_usec = 0;
    setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);

    
    struct sockaddr_in sa;
    sa.sin_family = AF_INET;
    sa.sin_port = htons(port);
    int ret = inet_pton(AF_INET, ip.c_str(), &(sa.sin_addr));
    
    fprintf(stderr, "connecting to remote server %s port %d\n", inet_ntoa(sa.sin_addr), port);

    ret = connect(fd, (struct sockaddr*)&sa, sizeof(sa));
    error_save = errno;

    // EINPROGRESS
    if(ret < 0 && error_save != EINPROGRESS) {
        fprintf(stderr, "failed connecting with [%d] : %s\n", error_save, strerror(error_save));
        return EXIT_FAILURE;
    }

    printf(">>> %s\n", command_str.c_str());

    command_str+="\r\n";



    size_t total = 0;  ssize_t left = command_str.size();
    while(total < command_str.size()) {
        int n = send(fd, command_str.c_str() + total, left, 0);
        if (n == -1) { 
            shutdown(fd, SHUT_RDWR); close(fd);
            return EXIT_FAILURE;
        }
        total += n; left -= n;
    }
    

    // Считываем ответ сервера
    char buf[1024];
    memset(buf,'\0',sizeof(buf));
    size_t n = recv(fd, buf, sizeof(buf) -1, MSG_NOSIGNAL);
    if (n <= 0) {
        fprintf(stderr, "failed recv\n");
        shutdown(fd, SHUT_RDWR); close(fd);
        return EXIT_FAILURE;
    }

    // выводим ответ сервера
    printf("%s",buf);
    
    shutdown(fd, SHUT_RDWR);
    close(fd);
    
    return EXIT_SUCCESS;
}

