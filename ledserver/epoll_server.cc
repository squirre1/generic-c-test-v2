#include "epoll_server.h"


#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/epoll.h>
#include <sys/ioctl.h>
#include <string.h>

#include <signal.h>
#include <sys/signalfd.h>


#define LISTEN_BACKLOG 10
#define THREAD_COUNT 10
#define MAX_EPOLL_EVENTS 32

#define BUFSIZE 1024


EpollServer::EpollServer()
    : m_commands()
    , m_commands_mutex()
    , m_server_socket(0)
    , m_epoll_fd(0)
    , m_threads()
{
    ;
}


/**
   Старая универсальная команда взятая из какого-то
   руководства и использующаяся везде
*/
int EpollServer::SetNonblock(int fd)
{
	int flags;
#if defined(O_NONBLOCK)
	if (-1 == (flags = fcntl(fd, F_GETFL, 0)))
		flags = 0;
	return fcntl(fd, F_SETFL, flags | O_NONBLOCK);
#else
	flags = 1;
	return ioctl(fd, FIONBIO, &flags);
#endif
}



void EpollServer::RegisterCommand(std::string _str, CommandPtr _command)
{
    m_commands.emplace(_str, _command);
}


void EpollServer::RegisterClient()
{
    int client_socket;
    struct sockaddr_in addr;
    socklen_t addrlen = sizeof(addr);
    if ((client_socket = accept(m_server_socket, (struct sockaddr *) &addr, &addrlen)) < 0) {
        perror("cannot accept client");
        return;
    }
    SetNonblock(client_socket);
    
    struct epoll_event epevent;
    epevent.events = EPOLLIN | EPOLLET;
    epevent.data.fd = client_socket;

    if (epoll_ctl(m_epoll_fd, EPOLL_CTL_ADD, client_socket, &epevent) < 0) {
        perror("epoll_ctl fail to add client socket");
        close(client_socket);
    }
}


// Немного экономил место чтобы метод влез в экран, надеюсь читаемость не потерялась
void EpollServer::ProcessRequest(int _fd) {
    std::string str;
    int n;

    // Считываем запрос пользователя
    char buf[BUFSIZE];
    while ((n = recv(_fd, buf, sizeof(buf) -1, MSG_NOSIGNAL)) > 0) {
        str.append(buf, n);
    }
    if (n < 0) {
        if (errno == EWOULDBLOCK || errno == EAGAIN) { /* no data, it's ok */  }
        else {
            shutdown(_fd, SHUT_RDWR); close(_fd);
            return;
        }
    }
    if (n == 0) {
        shutdown(_fd, SHUT_RDWR); close(_fd);
        return;
    }
    
    Request request(std::move(str));
    Responce responce;

    // Обрабатываем команду
    if (request.Size() <= 0) {
        responce = ""; // Человек просто нажал `enter`, ничего не отправляем
    }
    else {
        m_commands_mutex.lock();
        std::string_view sv = request.GetHeader();
        auto res = std::find_if(begin(m_commands), end(m_commands), [sv](const auto& value) {
                return value.first == sv;
            });
        m_commands_mutex.unlock();
        if (res == end(m_commands)) {
            responce = RESPONCE_FAILED("Command doesn't exist");
        }
        else {
            // Вызываем обработчик команды
            res->second(request, responce);
        }
    }
    
    // Отправляем ответ  - beej.us's sendall version
    size_t total = 0;  ssize_t left = responce.size();
    while(total < responce.size()) {
        n = send(_fd, responce.c_str() + total, left, 0);
        if (n == -1) { 
            shutdown(_fd, SHUT_RDWR); close(_fd);
            return;
        }
        total += n; left -= n;
    }
}


void EpollServer::ThreadLoop()
{
    auto events = std::make_unique<struct epoll_event[]>(MAX_EPOLL_EVENTS);
    int events_count;

    while ((events_count = epoll_wait(m_epoll_fd, events.get(), MAX_EPOLL_EVENTS, -1)) > 0) {
        for (int i = 0; i < events_count; i++) {
            if(events[i].events & EPOLLERR) {
                int       error = 0;
                socklen_t errlen = sizeof(error);
                if (getsockopt(events[i].data.fd, SOL_SOCKET, SO_ERROR, (void *)&error, &errlen) == 0) {
                    perror("EPOLLERR");
                    perror(strerror(error));
                }
                shutdown(events[i].data.fd, SHUT_RDWR);
                close(events[i] .data.fd);
                continue;
            }
            if(events[i].events & EPOLLHUP) {
                shutdown(events[i].data.fd, SHUT_RDWR);
                close(events[i] .data.fd);
                continue;
            }
            if (!(events[i].events & EPOLLIN)) {
                perror("MISSING EPOLLIN");
                continue;
            }
            
            if (events[i].data.fd == m_server_socket) {
                RegisterClient();
            }
            else {
                ProcessRequest(events[i].data.fd);
            }
        }
    }
    
    perror("epoll_wait error");
    // Пока не придумал как обрабатывать
}

void EpollServer::Run(int _port)
{   
    if ((m_server_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    struct sockaddr_in serveraddr;
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_port = htons(_port);
    serveraddr.sin_addr.s_addr = INADDR_ANY;
    
    // make reuse
    int reuse = 1;
    if (setsockopt(m_server_socket, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof (reuse)) < 0) {
        perror("setsockopt SO_REUSEADDR failed");
        exit(EXIT_FAILURE);
    }

    if (bind(m_server_socket, (const struct sockaddr *) &serveraddr, sizeof(serveraddr)) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
    
    SetNonblock(m_server_socket);
    
    if (listen(m_server_socket, LISTEN_BACKLOG) < 0) {
        perror("listen failed");
        exit(EXIT_FAILURE);
    }
    
    if ((m_epoll_fd = epoll_create(1)) < 0) {
        perror("epoll_create1 failed");
        exit(EXIT_FAILURE);
    }
    
    struct epoll_event epevent;
    epevent.events = EPOLLIN | EPOLLET;  // edge-trigger mode is necessary
    epevent.data.fd = m_server_socket;
    
    if (epoll_ctl(m_epoll_fd, EPOLL_CTL_ADD, m_server_socket, &epevent) < 0) {
        perror("epoll_ctl fail to add server socket");
        exit(EXIT_FAILURE);
    }
    
    // Блокируем SIGUSR1 и SIGUSR2
    signal(SIGUSR1, SIG_IGN);
    signal(SIGUSR2, SIG_IGN);
    
    for (size_t i = 0; i < THREAD_COUNT; i++) {
        m_threads.emplace_back(&EpollServer::ThreadLoop, this);
    }

    for (auto& t : m_threads) {
        t.join();
    }

}
