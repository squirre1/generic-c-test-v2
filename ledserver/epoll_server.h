#pragma once

#include <unordered_map>
#include <string>
#include <thread>
#include <functional>
#include <mutex>

#include "request.h"
#include "responce.h"


/**
   Многопоточный EPoll сервер обрабатывающий команды типа CommandPtr
*/
class EpollServer {
    
public:
    explicit EpollServer();
    
    /** Тип выполняемой команды */
    using CommandPtr = std::function<void(const Request&, Responce&)>;
    
    /** Запуск сервера */
    void Run(int _port);

protected:

    /** Регистрация команды для обработки */
    void RegisterCommand(std::string _str, CommandPtr _command);

private:

    /** Цикл каждого потока */
    void ThreadLoop();

    /** Регистрация нового сокета (accept) */
    void RegisterClient();

    /** Обработка запроса от сокета */
    void ProcessRequest(int _fd);

    /** Установка сокета в неблокирующий режим */
    static int SetNonblock(int fd);

    /** Зарегистрированные команды */
    std::unordered_map<std::string, CommandPtr> m_commands;
    
    /** Мьютекс для доступа к зарегистрированным командам */
    std::mutex m_commands_mutex;

    /** Серверный сокет */
    int m_server_socket;

    /** Дескриптор экземпляра EPoll */
    int m_epoll_fd;
    
    /** Контейнер с потоками */
    std::vector<std::thread> m_threads;
};
