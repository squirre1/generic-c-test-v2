#include "led_server.h"

#include <iostream>


#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/epoll.h>


#define LISTEN_BACKLOG 10
#define THREAD_COUNT 10
#define SERVERPORT 3000
#define MAX_EPOLL_EVENTS 32


LedServer::LedServer(Led *_led)
    : EpollServer()
    , m_led(_led)
    , displayLedThread(&LedServer::DisplayLed, this)
{
    using namespace std::placeholders;
    
    RegisterCommand("set-led-state", std::bind(&LedServer::SetLedState, this, _1, _2));
    RegisterCommand("get-led-state", std::bind(&LedServer::GetLedState, this, _1, _2));
    RegisterCommand("set-led-color", std::bind(&LedServer::SetLedColor, this, _1, _2));
    RegisterCommand("get-led-color", std::bind(&LedServer::GetLedColor, this, _1, _2));
    RegisterCommand("set-led-rate", std::bind(&LedServer::SetLedRate, this, _1, _2));
    RegisterCommand("get-led-rate", std::bind(&LedServer::GetLedRate, this, _1, _2));

    
}


LedServer::~LedServer()
{
    displayLedThread.join();
}


void LedServer::DisplayLed()
{
    while (true) {
        std::this_thread::sleep_for(std::chrono::milliseconds(3000) );
        std::cout << *m_led << std::endl;
    }
}


void LedServer::SetLedState(const Request &_request, Responce &_responce)
{
    if (_request.GetArgumentsSize() != 2) {
        _responce = RESPONCE_FAILED("wrong argument count");
        return;
    }
    const std::vector<std::string_view> &args = _request.GetArguments();
    Led::State state = Led::StateFromString(args.at(1));
    if (state == Led::State::UNKNOWN) {
        _responce = RESPONCE_FAILED("wrong state");
        return;
    }
    bool res = m_led->SetState(state);
    if (!res) {
        _responce = RESPONCE_FAILED("Led unavailible");
        return;
    }
    _responce = RESPONCE_OK();
}


void LedServer::SetLedColor(const Request &_request, Responce &_responce)
{
    if (_request.GetArgumentsSize() != 2) {
        _responce = RESPONCE_FAILED("wrong argument count");
        return;
    }
    const std::vector<std::string_view> &args = _request.GetArguments();
    Led::Color color = Led::ColorFromString(args.at(1));
    if (color == Led::Color::UNKNOWN) {
        _responce = RESPONCE_FAILED("wrong color");
        return;
    }
    bool res = m_led->SetColor(color);
    if (!res) {
        _responce = RESPONCE_FAILED("Led unavailible");
        return;
    }
    _responce = RESPONCE_OK();
}


void LedServer::SetLedRate(const Request &_request, Responce &_responce)
{
    if (_request.GetArgumentsSize() != 2) {
        _responce = RESPONCE_FAILED("wrong argument count");
        return;
    }
    const std::vector<std::string_view> &args = _request.GetArguments();
    unsigned short rate = Led::RateFromString(args.at(1));
    if (rate == Led::ERRORRATE) {
        _responce = RESPONCE_FAILED("wrong rate, try 0..5");
        return;
    }
    bool res = m_led->SetRate(rate);
    if (!res) {
        _responce = RESPONCE_FAILED("Led unavailible");
        return;
    }
    _responce = RESPONCE_OK();
}


void LedServer::GetLedState(const Request &_request, Responce &_responce)
{
    if (_request.GetArgumentsSize() != 1) {
        _responce = RESPONCE_FAILED("wrong argument count");
        return;
    }
    Led::State state = m_led->GetState();
    if (state == Led::State::UNKNOWN) {
        _responce = RESPONCE_FAILED();
        return;
    }
    _responce = RESPONCE_OK(Led::StateToString(state));
}


void LedServer::GetLedColor(const Request &_request, Responce &_responce)
{
    if (_request.GetArgumentsSize() != 1) {
        _responce = RESPONCE_FAILED("wrong argument count");
        return;
    }
    Led::Color color = m_led->GetColor();
    if (color == Led::Color::UNKNOWN) {
        _responce = RESPONCE_FAILED();
        return;
    }
    _responce = RESPONCE_OK(Led::ColorToString(color));
}


void LedServer::GetLedRate(const Request &_request, Responce &_responce)
{
    if (_request.GetArgumentsSize() != 1) {
        _responce = RESPONCE_FAILED("wrong argument count");
        return;
    }
    unsigned short rate = m_led->GetRate();
    if (rate == Led::ERRORRATE) {
        _responce = RESPONCE_FAILED();
        return;
    }
    _responce = RESPONCE_OK(Led::RateToString(rate));
}
