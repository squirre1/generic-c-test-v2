#include "led.h"

#include <unordered_map>
#include <algorithm>
#include <chrono>
#include <thread>


#define STATE_BLOCK_SEC 5


Led::Led()
    : m_color(Led::Color::GREEN)
    , m_rate(0)
    , m_state(Led::State::OFF)
    , m_state_mutex()
{
    ;
}

static const std::unordered_map<Led::Color, std::string> colors = {
    {Led::Color::UNKNOWN, "unknown"},
    {Led::Color::RED, "red"},
    {Led::Color::GREEN, "green"},
    {Led::Color::BLUE, "blue"},
};


// static
Led::Color Led::ColorFromString(std::string_view _str)
{
    auto it = std::find_if(begin(colors), end(colors),
                           [_str](const auto& _elem) { return _elem.second == _str; });
    if (it == end(colors))
        return Led::Color::UNKNOWN;
    return it->first;
}


// static
std::string Led::ColorToString(Led::Color _color)
{
    auto it = colors.find(_color);
    if (it == end(colors))
        return colors.at(Led::Color::UNKNOWN);
    return it->second;
}


// static
Led::State Led::StateFromString(std::string_view _str)
{
    if (_str == "on")
        return Led::State::ON;
    if (_str == "off")
        return Led::State::OFF;
    return Led::State::UNKNOWN;
}


// static
std::string Led::StateToString(Led::State _state)
{
    if (_state == Led::State::ON)
        return "on";
    if (_state == Led::State::OFF)
        return "off";
    return "unknown";
}


// static
bool Led::IsValidRate(unsigned short _rate)
{
    return _rate <= 5;
}


// static
unsigned short Led::RateFromString(std::string_view _str)
{
    unsigned short res;
    try {
        // dirty, because <charconv> header is not exist with my GCC
        res = std::stoi(std::string(_str));
    }
    catch(...) {
        res = Led::ERRORRATE;
    }
    if (!Led::IsValidRate(res))
        res = Led::ERRORRATE;
    return res;
}


// static
std::string Led::RateToString(unsigned short _rate)
{
    return Led::IsValidRate(_rate) ? std::to_string(_rate) : "ERROR";
}
    

Led::Color Led::GetColor() const
{
    if (m_state == Led::State::UNKNOWN)
        return Led::Color::UNKNOWN;
    return m_color;
}


Led::State Led::GetState() const
{
    return m_state;
}


unsigned short Led::GetRate() const
{
    if (m_state == Led::State::UNKNOWN)
        return Led::ERRORRATE;
    return m_rate;
}


bool Led::SetColor(Led::Color _color)
{
    if (m_state == Led::State::UNKNOWN)
        return false;
    m_color = _color;
    return true;
}


bool Led::SetState(Led::State _state)
{
    if (m_state_mutex.try_lock()) {
        m_state = Led::State::UNKNOWN;
    
        std::this_thread::sleep_for(std::chrono::milliseconds(STATE_BLOCK_SEC * 1000) );
        
        m_state = _state;
        m_state_mutex.unlock();
        return true;
    }
    else {
        // Уже идет изменение состояния
        return false;
    }
    
}


bool Led::SetRate(unsigned short _rate)
{
    if (m_state == Led::State::UNKNOWN || !IsValidRate(_rate))
        return false;
    m_rate = _rate;
    return true;
}
