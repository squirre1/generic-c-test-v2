#pragma once


/** Ответ - символьная строка */
using Responce = std::string;

/** Ответ с ошибкой */
inline std::string RESPONCE_FAILED(const std::string &_str = "") {
    if (_str.empty()) {
        return "FAILED\n";
    }
    return "FAILED " + _str + "\n";
}


/** Успешный ответ */
inline std::string RESPONCE_OK(const std::string &_str = "") {
    if (_str.empty()) {
        return "OK\n";
    }
    return "OK " + _str + "\n";
}


