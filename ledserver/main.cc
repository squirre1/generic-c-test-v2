#include <iostream>

#define SERVERPORT 3000

#include "led.h"
#include "led_server.h"

int main(int argc, char **argv)
{
    Led led;
    LedServer server(&led);
    server.Run(SERVERPORT);
    
    return EXIT_SUCCESS;
}
