#pragma once

#include <thread>

#include "epoll_server.h"
#include "request.h"
#include "responce.h"
#include "led.h"


/**
   Сервер управления эмулятором светодиода
   В конструкторе необходимо зарегистрировать команды через `RegisterCommand`
   И запустить его в работу через родительский EpollServer::Run
*/
class LedServer : public EpollServer {
    
public:
    
    explicit LedServer(Led *_led);
    ~LedServer();

    
    LedServer(const LedServer&) = delete;
    LedServer &operator=(const LedServer &) = delete;
    LedServer(const LedServer&&) = delete;
    LedServer &operator=(LedServer &&) = delete;
    

private:
    
    void SetLedState(const Request &_request, Responce &_responce);
    void SetLedColor(const Request &_request, Responce &_responce);
    void SetLedRate(const Request &_request, Responce &_responce);
    
    void GetLedState(const Request &_request, Responce &_responce);
    void GetLedColor(const Request &_request, Responce &_responce);
    void GetLedRate(const Request &_request, Responce &_responce);


    void DisplayLed();
    
    Led *m_led;

    std::thread displayLedThread;
    
};
