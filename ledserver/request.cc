#include "request.h"


std::string_view Request::GetHeader() const
{
    return m_arguments[0];
}


/*
  Не совсем точный метод, так как в нулевом элементе будет не аргумент,
  а сама функция, но на данном этапе я решил остановиться и не писать
  библиотеку для обработки запросов
*/
const std::vector<std::string_view> &Request::GetArguments() const
{
    return m_arguments;
}


size_t Request::GetArgumentsSize() const
{
    return m_arguments.size();
}


size_t Request::Size() const
{
    return m_str.size();
}


void trim_crlf(std::string &_str)
{
    if (_str.back() == 0x0a || _str.back() == 0x0d)
        _str.pop_back();
    if (_str.back() == 0x0a || _str.back() == 0x0d)
        _str.pop_back();
}


/*
  Тут можно было двигать указатель инициализированный из m_str.data()
  но мне показалось что этот подход ближе к языку C, а собеседование на C++
  
  Также есть общепринятый метод с использованием std::back_inserter, но
  он не умеет делать перемещение, а мне захотелось избежать копирования
  
  Поэтому было решено использовать `emplace_back` для конструирования
  на месте, простой std::string::find и немного арифметики
  
  Вообще, здесь, конечно, просится какая-нибудь библиотечная функция,
  писать велосипеды не очень приятно, пришлось контролировать дебагером
  что нигде не ошибся с инкрементом и не вышел за пределы

  Если будет добавлен пробел в конце или начале, или встретится 2 пробела,
  эти ситуации будут восприняты как пустые аргументы и всё равно будут
  добавлены с size=0, но т.к. задача искусственная не стал решать это
  ведь могут быть разные стратегии обработки
*/

void Request::Parse()
{
    m_arguments.clear();
    
    trim_crlf(m_str);
    
    size_t lastpos = 0;
    size_t pos =  m_str.find(' ', lastpos);
    
    while (pos != std::string::npos) {
        m_arguments.emplace_back(m_str.data() + lastpos, pos - lastpos);
        lastpos = pos + 1;
        pos =  m_str.find(' ', lastpos);
    }
    
    m_arguments.emplace_back(m_str.data() + lastpos, m_str.size() - lastpos);
}



