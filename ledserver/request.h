#pragma once

#include <string>
#include <string_view>
#include <vector>


/**
   Хранение строки запроса и получение её частей для обработки
*/

class Request {
    
public:
    
    explicit Request(std::string &&_str)
        : m_str(std::move(_str)) {
        Parse();
    }
    
    Request(const Request&) = delete;
    Request &operator=(const Request &) = delete;
    Request(const Request&&) = delete;
    Request &operator=(Request &&) = delete;

    /** Получить название команды */
    std::string_view GetHeader() const;

    /** Получить список аргументов (включая название команды) разделенных через пробел */
    const std::vector<std::string_view> &GetArguments() const;

    /** Получить количество аргументов (включая название команды) разделенных через пробел */
    size_t GetArgumentsSize() const;

    /** Получить длину строки запроса */
    size_t Size() const;
    
private:

    void Parse();
    
    std::string m_str;
    std::vector<std::string_view> m_arguments;
    
};
