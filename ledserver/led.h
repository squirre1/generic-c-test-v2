#pragma once

#include <string>
#include <vector>
#include <iostream>
#include <mutex>
#include <atomic>


/**
   Потокобезопасный эмулятор светодиода
*/
class Led {

public:

    explicit Led();
    
    Led(const Led&) = delete;
    Led &operator=(const Led &) = delete;
    Led(const Led&&) = delete;
    Led &operator=(Led &&) = delete;
    
    
    enum class Color {
        UNKNOWN = 0, RED = 0xA5, GREEN = 0xA6, BLUE = 0xA7
    };
    
    enum class State {
        UNKNOWN = 0, ON = 0xB0, OFF = 0xB1
    };

    /** Ошибочное значение частоты */
    static const unsigned short ERRORRATE = std::numeric_limits<unsigned short>::max();

    /** Возвращает цвет. Если в данный момент идёт изменение состояния, то вернет UNKNOWN */
    Color GetColor() const;

    /** Возвращает состояние */
    State GetState() const;

    /** Возвращает частоту. Если в данный момент идёт изменение состояния, то вернет UNKNOWN */
    unsigned short GetRate() const;
    
    /** Устанавливает цвет. Если в данный момент идёт изменение состояния, то вернет false */
    bool SetColor(Color _color);

    /**
       Устанавливает cостояние некоторое время.
       Если в данный момент уже идет изменение состояния, то вернет false
    */
    bool SetState(State _state);

    /** Устанавливает частоту. Если в данный момент идёт изменение состояния, то вернет false */
    bool SetRate(unsigned short _rate);
    
    
    static Color ColorFromString(std::string_view _str);
    static std::string ColorToString(Color _color);
    
    static State StateFromString(std::string_view _str);
    static std::string StateToString(State _state);
    
    static bool IsValidRate(unsigned short _rate);
    static unsigned short RateFromString(std::string_view _str);
    static std::string RateToString(unsigned short _rate);


private:

    /** Текущий цвет светодиода */
    std::atomic<Color> m_color;

    /** Частота мерцания */
    std::atomic<unsigned short> m_rate;

    /** Текущее состояние светодиода */
    std::atomic<State> m_state;

    /** Мьютекс для блокировки повторного изменения состояния */
    std::mutex m_state_mutex;
    
    /** Текстовый вывод объекта в поток */
    friend std::ostream & operator<<(std::ostream &os, const Led& _led);
};


inline std::ostream& operator<<(std::ostream& os,
                                const Led::Color& _color)
{
    os << Led::ColorToString(_color);
    return os;
}


inline std::ostream& operator<<(std::ostream& os,
                                const Led::State& _state)
{
    os << Led::StateToString(_state);
    return os;
}

inline std::ostream& operator<<(std::ostream& os,
                                const Led& _led)
{
    os << "[LED] color=" << _led.m_color << " "
       << "state=" << _led.m_state << " "
       << "rate=" << Led::RateToString(_led.m_rate);
    return os;
}
